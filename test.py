import cv2

# Load the pre-trained model for traffic sign detection
cascade_classifier = cv2.CascadeClassifier('casace5.xml')

# Open the video file
cap = cv2.VideoCapture('DayDrive1.mp4')

# Check if the video file was opened successfully
if not cap.isOpened():
    print("Error opening video file")
    exit()

# Loop through each frame of the video
while True:
    # Read the next frame from the video file
    ret, frame = cap.read()

    # If there are no more frames to read, break out of the loop
    if not ret:
        break

    frame = cv2.resize(frame, (640,480))
    
    # Convert the frame to grayscale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Detect traffic signs in the grayscale frame using the cascade classifier
    signs = cascade_classifier.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5)
    
    # Draw a rectangle around each detected sign
    for (x, y, w, h) in signs:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
    
    # Display the resulting frame
    cv2.imshow('frame', frame)

    # Exit the loop if the user presses the 'q' key
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
        
# Release the video file and close all windows
cap.release()
cv2.destroyAllWindows()