import cv2

# Global variables
current_frame = 0
frame_step = 1

# Function to update the current frame
def update_frame_position(position):
    global current_frame
    current_frame = position

# Load video file
video_path = "path/to/video.mp4"
cap = cv2.VideoCapture('DayDrive1.mp4')
# Get total number of frames in the video
total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

# Load cascade classifiers
cascade_files = [
    "stopcascade.xml",
    #"roicascade.xml",
    "circlecascade.xml",
    "triangularcascade.xml",
    "traffic_sign_cascade.xml"
]

cascades = []
for cascade_file in cascade_files:
    cascade = cv2.CascadeClassifier(cascade_file)
    cascades.append(cascade)

# Create named window
cv2.namedWindow("Video")

# Create trackbar for frame navigation
cv2.createTrackbar("Position", "Video", 0, total_frames-1, update_frame_position)

# Process video frames
while cap.isOpened():
    # Set the video capture to the current frame position
    cap.set(cv2.CAP_PROP_POS_FRAMES, current_frame)

    # Read the frame
    ret, frame = cap.read()
    if not ret:
        break

    #frame = cv2.resize(frame, (900,600))

    # Convert frame to grayscale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Apply cascade classifiers
    detections = []
    for cascade in cascades:
        objects = cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))
        detections.extend(objects)

    # Draw bounding boxes on the frame
    for (x, y, w, h) in detections:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

    # Get frame dimensions
    height, width, _ = frame.shape

    # Calculate the new height after cutting 30% from the bottom
    new_height = int(height * 0.7)  # 70% of the original height

    # Crop the frame by resizing its height
    cropped_frame = frame[:new_height, :]

    # Display cropped frame
    cv2.imshow("Video", cropped_frame)

    # Check for keyboard events
    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        break
    elif key == ord('n'):  # Button for forward 10 frames
        current_frame += 10
        current_frame = min(current_frame, total_frames-1)
        cv2.setTrackbarPos("Position", "Video", current_frame)
    elif key == ord('m'):  # Button for forward 20 frames
        current_frame += 20
        current_frame = min(current_frame, total_frames-1)
        cv2.setTrackbarPos("Position", "Video", current_frame)
    elif key == ord(','):  # Button for forward 30 frames
        current_frame += 30
        current_frame = min(current_frame, total_frames-1)
        cv2.setTrackbarPos("Position", "Video", current_frame)
    elif key == ord('.'):  # Button for forward 30 frames
        current_frame -= 30
        current_frame = min(current_frame, total_frames-1)
        cv2.setTrackbarPos("Position", "Video", current_frame)

# Release resources
cap.release()
cv2.destroyAllWindows()